# Dollar Example App
Dollar is a simple and quick application with current exchange rates from the Central Bank of Russia.

Technology Stack: Xcode, SwiftUI, MVVM, XML parsing.

Created by Daniil Belikov.

# Preview:
<img src="./Assets/preview.png"/>
