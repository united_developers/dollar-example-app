//
//  CoursesServiceProtocol.swift
//  Dollar
//
//  Created by Daniil Belikov on 13.08.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import Foundation

public protocol CoursesServiceProtocol {
    
    func getCourses(onDate: Date?,
                    completion: @escaping ((Result<[CoursesModel], Error>), String) -> Void)
    
}
