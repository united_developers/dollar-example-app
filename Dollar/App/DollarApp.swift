//
//  DollarApp.swift
//  Dollar
//
//  Created by Daniil Belikov on 30.07.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import Services
import SwiftUI

@main
struct DollarApp: App {
    
    var body: some Scene {
        WindowGroup {
            TabNavigationView()
        }
    }
    
}
