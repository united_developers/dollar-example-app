//
//  String+Localization.swift
//  Dollar
//
//  Created by Daniil Belikov on 17.08.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import Foundation

extension String {
    
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}
