//
//  AppColor.swift
//  Dollar
//
//  Created by Daniil Belikov on 30.07.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import Foundation

enum AppColor {
    
    enum Common {
        // light: hex #F4AA3A, rgb 244 170 58
        @DynamicColor(light: #colorLiteral(red: 0.957, green: 0.667, blue: 0.227, alpha: 1)) static var tint
        
        // light: hex #0D0D0D, rgb 13 13 13
        @DynamicColor(light: #colorLiteral(red: 0.051, green: 0.051, blue: 0.051, alpha: 1)) static var background
        
        // light: hex #1F1F1F, rgb 31 31 31
        @DynamicColor(light: #colorLiteral(red: 0.122, green: 0.122, blue: 0.122, alpha: 1)) static var navigationBar
        
        // light: hex #FFFFFF, rgb 255 255 255
        @DynamicColor(light: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) static var whiteText
        
        // light: hex #E6E6E6, rgb 230 230 230
        @DynamicColor(light: #colorLiteral(red: 0.901, green: 0.901, blue: 0.901, alpha: 1)) static var grayText
    }
    
}
