//
//  CoursesMapperProtocol.swift
//  Dollar
//
//  Created by Daniil Belikov on 10.08.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import Foundation
import Services

protocol CoursesMapperProtocol {
    
    func parse(result: Result<[CoursesModel], Error>) -> [CourseCellModel]?
    
}
