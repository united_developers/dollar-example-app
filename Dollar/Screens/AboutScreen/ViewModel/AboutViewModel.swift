//
//  AboutViewModel.swift
//  Dollar
//
//  Created by Daniil Belikov on 20.09.2021.
//  Copyright © 2021 belikov.dev. All rights reserved.
//

import UIKit

final class AboutViewModel: ObservableObject {
    
    init(){}
    
}

// MARK: - Open Web Browser

extension AboutViewModel {
    
    func openWebSite() {
        if let url = URL(string: "https://udev.dev"),
           UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
}
