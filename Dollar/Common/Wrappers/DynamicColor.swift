//
//  DynamicColor.swift
//  Dollar
//
//  Created by Daniil Belikov on 31.07.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import SwiftUI

@propertyWrapper
struct DynamicColor {
    
    let light: UIColor
    let dark: UIColor?
    
    var wrappedValue: Color {
        if #available(iOS 13.0, *) {
            let color = UIColor { (traitCollection) -> UIColor in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return dark ?? light
                case .light, .unspecified:
                    return light
                @unknown default:
                    return light
                }
            }
            return Color(color)
        }
        return Color(light)
    }
    
    init(light: UIColor,
         dark: UIColor? = nil) {
        
        self.light = light
        self.dark = dark
    }
    
}
