//
//  NavigationViewColor.swift
//  Dollar
//
//  Created by Daniil Belikov on 31.07.2021.
//  Copyright © 2021 United Developers. All rights reserved.
//

import SwiftUI

struct NavigationViewColor: ViewModifier {
    
    init() {
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithOpaqueBackground()
        coloredAppearance.backgroundColor = UIColor(AppColor.Common.navigationBar)
        coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        
        UINavigationBar.appearance().standardAppearance = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
        UINavigationBar.appearance().compactAppearance = coloredAppearance
    }
    
    func body(content: Content) -> some View {
        content
    }
    
}
